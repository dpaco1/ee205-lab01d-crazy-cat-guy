///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - Crazy Cat Guy - EE 205 - Spr 2022
///
/// @file    crazyCatGuy.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o crazyCatGuy crazyCatGuy.c
///
/// Usage:  crazyCatGuy n
///   n:  Sum the digits from 1 to n
///
/// Result:
///   The sum of the digits from 1 to n is XX
///
/// Example:
///   $ summation 6
///   The sum of the digits from 1 to 6 is 21
///
/// author  David Paco <dpaco@hawaii.edu>
/// date    13/1/2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char *argv[] ) 
{
   int n = atoi( argv[1] );
   int sum = 0;
   int year;
   for(year = 1; year <= n; year = year + 1)
   {
      sum = sum + year;
   }
   printf("Crazy Cat Guy has bought %d collars in %d years\n", sum, n);
   return 0;
}
